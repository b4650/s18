/*function trainer(name,age,pokemon,friends){
	this.name = name,
	this.age = age,
	this.pokemon = pokemon,
	this.friends = friends
};

let kantino = new trainer("Kantino", 25, ["A", "B", "C"], ["A", "B", "C"]);

console.log(kantino)*/

let trainer = {
	name: "Kantino",
	age: 25,
	pokemon: ["Mew", "Mewtwo", "Articuno"],
	friends: {
		Adriel: ["Ana", "JP", "Charles"],
		Gab: ["Ann", "Gepee", "Maharlika"],
		Ryan: ["Marites", "Clarissa", "Josephine"]
	},
	talk: function(){
		console.log(trainer.pokemon[1] + " I choose you!")
	}
}

function pokemon(name,level){
	this.name = name,
	this.level = level,
	this.health = level*100,
	this.attack = level*50,
	this.tackle = function(target){
		if (target.health - this.attack <= 0){
			console.log(this.name + " tackled " + target.name)
			console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
			console.log(`${target.name} fainted`)
		}
		else {
			console.log(this.name + " tackled " + target.name)
			console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
		}
	}
}

let mew = new pokemon("Mew",5)
let mewtwo = new pokemon("Mewtwo", 6)
let articuno = new pokemon("Articuno", 20)

